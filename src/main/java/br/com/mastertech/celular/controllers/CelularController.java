package br.com.mastertech.celular.controllers;

import br.com.mastertech.celular.models.Celular;
import br.com.mastertech.celular.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CelularController {

    @GetMapping("/{marca}/{modelo}")
    public Celular getCelular(@PathVariable(name = "marca") String marca,
                              @PathVariable(name = "modelo") String modelo, @AuthenticationPrincipal Usuario usuario){
        Celular celular = new Celular();
        celular.setMarca(marca);
        celular.setModelo(modelo);
        celular.setDono(usuario.getName());
        return celular;
    }
}
